----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.01.2020 17:06:15
-- Design Name: 
-- Module Name: cafe_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.Std_logic_1164.all;

entity fsm_tb is
end fsm_tb;

architecture behavioral of fsm_tb is

  component fsm
      Port ( clk : in STD_LOGIC;
             rst : in STD_LOGIC;
             input : in STD_LOGIC;
             encendido : in STD_LOGIC;
             corto : in STD_LOGIC;
             largo : in STD_LOGIC;
             leche : in STD_LOGIC;
             leche_fria : in STD_LOGIC;
             leche_caliente : in STD_LOGIC; 
             azucar : in STD_LOGIC;
             mas_azucar : in STD_LOGIC;
             menos_azucar : in STD_LOGIC;
             led_corto : out STD_LOGIC;
             led_largo : out STD_LOGIC;
             led_leche : out STD_LOGIC;
             led_azucar : out STD_LOGIC;
             led_encendido : out STD_LOGIC;
             led_apagado : out STD_LOGIC
            );
  end component;

  signal clk: STD_LOGIC := '0';
  signal rst: STD_LOGIC := '0';
  signal input: STD_LOGIC := '0';
  signal encendido: STD_LOGIC := '0';
  signal corto: STD_LOGIC := '0';
  signal largo: STD_LOGIC := '0';
  signal leche: STD_LOGIC := '0'; --hasta aqui entradas
  signal leche_fria: STD_LOGIC := '0'; --hasta aqui entradas
  signal leche_caliente: STD_LOGIC := '0'; --hasta aqui entradas
  signal azucar: STD_LOGIC := '0';
  signal mas_azucar: STD_LOGIC := '0';
  signal menos_azucar: STD_LOGIC := '0';
  signal led_corto: STD_LOGIC;
  signal led_largo: STD_LOGIC;
  signal led_leche: STD_LOGIC;
  signal led_azucar: STD_LOGIC;
  signal led_encendido: STD_LOGIC;
  signal led_apagado: STD_LOGIC;
  --signal salida: STD_LOGIC_VECTOR (3 DOWNTO 0) ; --hasta aqui salidas

  constant clock_period: time := 100 ns;
  --signal stop_the_clock: boolean;

begin

  uut: fsm port map (  clk           => clk,
                       rst           => rst,
                       input         => input,
                       encendido     => encendido,
                       corto         => corto,
                       largo         => largo,
                       leche         => leche, --hasta aqui entradas
                       leche_fria    => leche_fria,
                       leche_caliente=> leche_caliente,
                       azucar        => azucar,
                       mas_azucar    => mas_azucar,
                       menos_azucar  => menos_azucar,
                       led_corto     => led_corto,
                       led_largo     => led_largo,
                       led_leche     => led_leche,
                       led_azucar    => led_azucar,
                       led_encendido => led_encendido,
                       led_apagado   => led_apagado);
                       
  clocking: process
  begin
      clk <= '0';
      wait for clock_period/2;
      clk <= '1';
      wait for clock_period/2;
  end process;

  stimulus: process
  begin
    rst <= '0';
    wait for 50ns;
    rst <= '1';
    wait for 50ns;
    
    rst <= '0';
    wait for 100ns;
    --cafe corto con leche y dos niveles de azucar
    encendido <= '1';
    wait for 100ns;
    corto <= '1';
    wait for 150ns;
    leche <= '1';
    wait for 50ns;
    azucar <= '1';
    wait for 2000ns;
    mas_azucar <= '1';
    wait for 250ns; --segundo nivel de azucar
    mas_azucar <= '0';
    wait for 2500ns;
    input <= '1';
    wait for 150 ns;
    input <= '0';
    wait for 55ns;
    corto <= '0';
    wait for 50 ns;
    leche <= '0';
    wait for 10000 ns;
    encendido <= '0';
    wait for 600 ns;
    
    assert false
    report "FIN"
    severity failure;    
  end process;
end behavioral;