----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.02.2021 18:36:57
-- Design Name: 
-- Module Name: cafetera_tb_real - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cafetera_tb_real is
--  Port ( );
end cafetera_tb_real;

architecture Behavioral of cafetera_tb_real is
    component cafetera   
        port(
           clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           input : in STD_LOGIC; --transiciones, start
           encendido : in STD_LOGIC;
           corto : in STD_LOGIC;
           largo : in STD_LOGIC;
           leche : in STD_LOGIC;
           leche_fria: in std_logic;
           leche_caliente: in std_logic;
           azucar : in STD_LOGIC;
           mas_azucar : in STD_LOGIC;
           menos_azucar : in STD_LOGIC;
           led_corto : out STD_LOGIC;
           led_largo : out STD_LOGIC;
           led_leche : out STD_LOGIC;
           led_azucar: out std_logic;
           led_encendido : out STD_LOGIC;
           led_apagado : out STD_LOGIC;
           led_leche_caliente: out STD_LOGIC;
           led_leche_fria: out STD_LOGIC;
           D_NUMBER:   out std_logic_vector(6 downto 0);
           D_SELECT:   out std_logic_vector(7 downto 0)
       );
    end component;   
    
   signal clk :  STD_LOGIC;
   signal rst :  STD_LOGIC;
   signal input :  STD_LOGIC; --transiciones, start
   signal encendido :  STD_LOGIC;
   signal corto :  STD_LOGIC;
   signal largo :  STD_LOGIC;
   signal leche :  STD_LOGIC;
   signal leche_fria:  std_logic;
   signal leche_caliente:  std_logic;
   signal azucar :  STD_LOGIC;
   signal mas_azucar :  STD_LOGIC;
   signal menos_azucar :  STD_LOGIC;
   signal led_corto :  STD_LOGIC;
   signal led_largo :  STD_LOGIC;
   signal led_leche :  STD_LOGIC;
   signal led_azucar:  std_logic;
   signal led_encendido :  STD_LOGIC;
   signal led_apagado :  STD_LOGIC;
   signal led_leche_caliente:  STD_LOGIC;
   signal led_leche_fria:  STD_LOGIC;
   signal D_NUMBER:    std_logic_vector(6 downto 0);
   signal D_SELECT:    std_logic_vector(7 downto 0) ;
   
   signal clk_period : time := 10 ns;
   
begin
uut: cafetera
    port map(
        clk => clk,
        rst => rst,
        input => input,
        encendido => encendido,
        corto => corto,
        largo => largo,
        leche => leche,
        leche_fria => leche_fria,
        leche_caliente => leche_caliente,
        azucar => azucar,
        mas_azucar => mas_azucar,
        menos_azucar => menos_azucar,
        led_corto => led_corto,
        led_largo => led_largo,
        led_leche => led_leche,
        led_azucar => led_azucar,
        led_encendido => led_encendido,
        led_apagado => led_apagado,
        led_leche_caliente => led_leche_caliente,
        led_leche_fria => led_leche_fria,
        D_NUMBER => D_NUMBER,
        D_SELECT => D_SELECT
    );
    
clk_process_100MHz: process
    begin
        clk <= '0';
        wait for clk_period/2;
        clk <= '1';
        wait for clk_period/2;
    end process;
    
stimulus: process
  begin
    rst <= '0';
    wait for 50ms;
    rst <= '1';
    wait for 50ms;
    rst <= '0';
    wait for 50ms;
    --cafe corto con leche y dos niveles de azucar
    input <= '0';
    encendido <= '1';
    mas_azucar <= '0';
    menos_azucar <='0';
    wait for 100ms;
    corto <= '1';
    largo <= '0';
    wait for 150ms;
    leche <= '1';
    leche_caliente <= '1';
    leche_fria <= '0';
    wait for 150ms;
    azucar <= '1';
    wait for 100ms;
    mas_azucar <= '1';
    menos_azucar <='0';
    wait for 50ms;
    mas_azucar <= '0';
    menos_azucar <='0';
    wait for 100ms;
    mas_azucar <= '0';
    menos_azucar <='1';
    wait for 50ms;
    mas_azucar <= '0';
    menos_azucar <='0';

    wait for 100ms;
    input <= '1';
    wait for 50 ms;
    input <= '0';
    wait for 25 ms;
    corto <= '0';
    leche <= '0';
    leche_caliente <= '0';
    leche_fria <= '0';
    wait for 11000 ms;

    
    
    assert false
    report "FIN"
    severity failure;    
  end process;

end Behavioral;
