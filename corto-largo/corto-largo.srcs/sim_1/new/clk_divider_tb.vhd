----------------------------------------------------------------------------------
-- TESTBENCH INDIVIDUAL DEL DIVISOR DE FRECUENCIA
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_divider_tb is
end clk_divider_tb;

architecture Behavioral of clk_divider_tb is
    component clk_divider
    Port ( 
        INTERNAL_CLK:  in std_logic;
        RESET:         in std_logic;
        OUTPUT_CLK:   out std_logic
    );
    end component;
    --Entradas
        signal INTERNAL_CLK: std_logic;
        signal RESET:        std_logic;
    -- Salida
        signal OUTPUT_CLK:   std_logic;
    -- Periodo del reloj
        constant CLK_PERIOD: time := 10 ns; -- 100 MHz
begin
    -- Instancia de la unidad bajo prueba.
    uut: clk_divider PORT MAP (
        INTERNAL_CLK => INTERNAL_CLK,
        RESET        => RESET,
        OUTPUT_CLK   => OUTPUT_CLK
    );
    -- Definici�n del reloj.
    reloj_entrada :process
        begin
        INTERNAL_CLK <= '0';
        wait for CLK_PERIOD / 2;
        INTERNAL_CLK <= '1';
        wait for CLK_PERIOD / 2;
    end process;
    -- Procesamiento de est�mulos.
    estimulos: process
    begin
        wait for 5 ns;
        RESET <= '1'; -- Condiciones iniciales.
        wait for 20 ns;
        RESET <= '0'; -- �A trabajar!
        wait for 300 ns;
        
        assert false
        report "FIN. SIMULACION CORRECTA."
        severity failure;
    end process;

end Behavioral;
