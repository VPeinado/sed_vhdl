----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.01.2021 13:31:25
-- Design Name: 
-- Module Name: debouncer_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity debouncer_tb is
end debouncer_tb;

architecture Behavioral of debouncer_tb is
    component debouncer is
        Port ( 
            CLK:        in std_logic;
            BOTON_IN:   in std_logic;
            BOTON_OUT:  out std_logic
        );
    end component;

    signal CLK: std_logic;
    signal BOTON_IN: std_logic;
    signal BOTON_OUT: std_logic;

    constant clock_period: time:=10ns;--100MHz

begin
    uut: debouncer
        port map(
            CLK => CLK,
            BOTON_IN => BOTON_IN,
            BOTON_OUT => BOTON_OUT
        );
         
    clock: process
    begin
        CLK <= '0';
        wait for clock_period/2;
        CLK <= '1';
        wait for clock_period/2;
    end process; 
    
    simulation:process
    begin
        wait for clock_period/4;
        BOTON_IN <= '1';
        wait for clock_period/2;
        BOTON_IN <= '0';
        wait for clock_period * 10;
        
        
        assert false
        report "FIN"
        severity failure;
    end process;
 
end Behavioral;
