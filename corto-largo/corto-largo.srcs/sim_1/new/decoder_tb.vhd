----------------------------------------------------------------------------------
-- TESTBENCH INDIVIDUAL DEL DECODIFICADOR
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity decoder_tb is
end decoder_tb;

architecture Behavioral of decoder_tb is

    COMPONENT decoder
    PORT(
        CODE :  in std_logic_vector(3 downto 0);
        LED  : out std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    -- Entradas
    signal CODE: std_logic_vector(3 downto 0);
    -- Salida
    signal LED:  std_logic_vector(6 downto 0);
    -- Vector de comprobacion
    TYPE vtest is record
        CODE:    std_logic_vector(3 downto 0);
        LED:     std_logic_vector(6 downto 0);
    END RECORD;
    
    TYPE vtest_vector is array (natural range <>) of vtest;
    constant test: vtest_vector := (
        (code => "0000", led => "1000000"), -- "0"
        (code => "0001", led => "1111001"), -- "1"
        (code => "0010", led => "0100100"), -- "2"
        (code => "0011", led => "0110000"), -- "3"
        (code => "0100", led => "1001111"), -- "I"
        (code => "0101", led => "0101111"), -- "r"
        (code => "0110", led => "0001100"), -- "P"
        (code => "0111", led => "0010010"), -- "S"
        (code => "1000", led => "0000110"), -- "E"
        (code => "1001", led => "0001000"), -- "A"
        (code => "1010", led => "0111111"), -- "-"
        (code => "1011", led => "1111111"), -- " "
        (code => "1100", led => "1000111"), -- "L"
        (code => "1101", led => "1000110"), -- "C"
        (code => "1110", led => "0000110"), -- "E"
        (code => "1111", led => "0001110")  -- "F"
        );
        
BEGIN
    -- Instancia de la unidad bajo prueba.
    uut_decoder: decoder 
    PORT MAP(
        code => code,
        led  => led
        );
    -- Procesamiento de estímulos.
    stim_proc: process
    BEGIN
        for i in 0 to test'HIGH loop
        code <= test(i).code;
            wait for 20 ns;
            assert led = test(i).led
            report "SALIDA INCORRECTA."
            severity failure;
        end loop;
        
        wait for 30 ns;
                
        assert false
        report "FIN. SIMULACION CORRECTA."
        severity failure;
    end process;
end Behavioral;
