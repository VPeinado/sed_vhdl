
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity display_refresh_tb is
end display_refresh_tb;

architecture Behavioral of display_refresh_tb is
    
    component display_refresh is
      Port ( 
            CLK:        in std_logic;
            RESET:      in std_logic;
            L_0:        in std_logic_vector(6 downto 0);
            L_1:        in std_logic_vector(6 downto 0);
            L_2:        in std_logic_vector(6 downto 0);
            L_3:        in std_logic_vector(6 downto 0);
            L_4:        in std_logic_vector(6 downto 0);
            L_5:        in std_logic_vector(6 downto 0);
            L_6:        in std_logic_vector(6 downto 0);
            L_7:        in std_logic_vector(6 downto 0);
            D_NUMBER:   out std_logic_vector(6 downto 0);
            D_SELECT:   out std_logic_vector(7 downto 0)
        );
    end component;
    
    signal CLK:         std_logic;
    signal RESET:       std_logic;
    signal L_0:         std_logic_vector(6 downto 0);
    signal L_1:         std_logic_vector(6 downto 0);
    signal L_2:         std_logic_vector(6 downto 0);
    signal L_3:         std_logic_vector(6 downto 0);
    signal L_4:         std_logic_vector(6 downto 0);
    signal L_5:         std_logic_vector(6 downto 0);
    signal L_6:         std_logic_vector(6 downto 0);
    signal L_7:         std_logic_vector(6 downto 0);
    signal D_NUMBER:    std_logic_vector(6 downto 0);
    signal D_SELECT:    std_logic_vector(7 downto 0);
    
    constant clock_period: time := 2.5 ms;

begin
    uut: display_refresh
        port map(
                    CLK     =>CLK,
                    RESET   =>RESET,
                    L_0     =>L_0,
                    L_1     =>L_1,
                    L_2     =>L_2,
                    L_3     =>L_3,
                    L_4     =>L_4,
                    L_5     =>L_5,
                    L_6     =>L_6,
                    L_7     =>L_7,
                    D_NUMBER=>D_NUMBER,
                    D_SELECT=>D_SELECT   
        );
    
clocking: process
    begin
        clk <= '0';
        wait for clock_period/2;
        clk <= '1';
        wait for clock_period/2;
    end process;

stimulus: process
  begin
    RESET <= '0';
    wait for 5ms;
    RESET <= '1';
    wait for 5ms;
    RESET <= '0';
    
    wait for 20ms;
    L_0 <= "1111110";
    L_1 <= "1111101";
    L_2 <= "1111011";
    L_3 <= "1110111";
    L_4 <= "1101111";
    L_5 <= "1011111";
    L_6 <= "0111111";
    L_7 <= "0111111";
    wait for 20ms;
    RESET <= '1';
    

    wait for 5 ms;
    
    assert false
    report "FIN"
    severity failure;    
  end process;



end Behavioral;
