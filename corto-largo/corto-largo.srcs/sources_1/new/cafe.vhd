----------------------------------------------------------------------------------
-- FSM (FINITE STATE MACHINE)
--Su funci�n es gestionar en todo momento el estado en el que se encuentra la cafetera. Va variando entre apagado, reposo y sirviendo
--Por tanto, partiremos del estado apagado
--Cuando encendido sea 1 y rst (reset) 0, la cafetera se encender� y pasar� al estado de reposo
--En reposo estar� a la espera de la introducci�n de alguna entrada, bien sea corto, largo, leche...
--Y por ultimo, una vez que la cafetera empiece una cuenta atr�s, estar� en el estado sirviendo, que una vez finalizada dicha cuenta
--pasar� a reposo de nuevo.
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity fsm is
    Port ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           input : in STD_LOGIC; --transiciones, start
           encendido : in STD_LOGIC;
           corto : in STD_LOGIC;
           largo : in STD_LOGIC;
           leche : in STD_LOGIC;
           leche_fria : in STD_LOGIC;
           leche_caliente : in STD_LOGIC; 
           azucar : in STD_LOGIC;
           mas_azucar : in STD_LOGIC;
           menos_azucar : in STD_LOGIC;
           led_corto : out STD_LOGIC;
           led_largo : out STD_LOGIC;
           led_leche : out STD_LOGIC;
           led_leche_caliente : out STD_LOGIC;
           led_leche_fria : out STD_LOGIC;
           led_azucar : out STD_LOGIC;
           led_encendido : out STD_LOGIC;
           led_apagado : out STD_LOGIC;
           L_0 : out STD_LOGIC_VECTOR(3 downto 0); --corto
           L_1 : out STD_LOGIC_VECTOR(3 downto 0); --largo
           L_2 : out STD_LOGIC_VECTOR(3 downto 0); --leche
           L_3 : out STD_LOGIC_VECTOR(3 downto 0); --leche fria o caliente
           L_4 : out STD_LOGIC_VECTOR(3 downto 0); --Azucar
           L_5 : out STD_LOGIC_VECTOR(3 downto 0); --Nivel Azucar
           L_6 : out STD_LOGIC_VECTOR(3 downto 0); --Libre
           L_7 : out STD_LOGIC_VECTOR(3 downto 0)  --Libre
          );
end fsm;

architecture Behavioral of fsm is

--estados cafetera
type estado is (inicio, reposo, dispensando); --inicio: maquina en estado inicial "apagada"
signal estado_actual, estado_siguiente : estado := inicio;

--tiempo, tipo de cafe, contador
type tipo_cafe is (inicio_cafe, select_cafe, cafe_corto, cafe_largo, solo_leche);
signal tiempo_cafe, tiempo_cafe_next : tipo_cafe := inicio_cafe;
signal count : integer range 0 to 2000000000 := 0; --contador caf�
signal L0, L1 : integer range 0 to 10 := 10; --led corto, largo

 

--leds
signal led_encendido_i, led_apagado_i : STD_LOGIC := '0';
signal led_corto_i, led_largo_i, led_corto_i_next, led_largo_i_next : STD_LOGIC := '0';
signal servido : STD_LOGIC := '0'; --cafe servido

--leche
type estado_leche is (inicio_leche, select_leche);
signal estado_leche_actual, estado_leche_next : estado_leche := inicio_leche;
signal led_leche_i, led_leche_next_i : STD_LOGIC := '0';
signal led_leche_caliente_i, led_leche_caliente_next_i, led_leche_fria_i, led_leche_fria_next_i : STD_LOGIC := '0';
signal leche_actual, leche_next : integer range 0 to 3 := 0; -- leche, leche caliente, leche fria (displays)

--azucar
type estado_azucar is (inicio_azucar, select_azucar);
signal estado_azucar_actual, estado_azucar_next : estado_azucar := inicio_azucar;
signal led_azucar_i, led_azucar_next_i : STD_LOGIC := '0';
signal nivel_azucar, nivel_azucar_next : integer range 0 to 3 := 0;

--Decodificador
signal binC : STD_LOGIC_VECTOR (3 downto 0) := "1011"; --corto
signal binL : STD_LOGIC_VECTOR (3 downto 0) := "1011"; --largo
signal binLeche : STD_LOGIC_VECTOR (3 downto 0) := "1011"; --leche
signal binL_C_F : STD_LOGIC_VECTOR (3 downto 0) := "1011"; --leche caliente
signal binA : STD_LOGIC_VECTOR (3 downto 0) := "1011"; --nivel 1 AZ
signal binA_nivel : STD_LOGIC_VECTOR (3 downto 0) := "1011"; --nivel 2 AZ


begin
    --registro de estado. reset
    --registro_estado: 
    process (rst, clk, encendido)
       begin
            if (rst = '1' or encendido = '0') then
                estado_actual <= inicio;
                led_corto_i <= '0';
                led_largo_i <= '0';
                tiempo_cafe <= inicio_cafe;
                estado_leche_actual <= inicio_leche;
                led_leche_i <= '0';
                leche_actual <= 0;
                led_leche_caliente_i <= '0';
                led_leche_fria_i <= '0';
                led_azucar_i <= '0';
                estado_azucar_actual <= inicio_azucar;
                nivel_azucar <= 0;
            elsif (clk = '0' and clk'event) then
                estado_actual <= estado_siguiente;
                led_corto_i <= led_corto_i_next;
                led_largo_i <= led_largo_i_next;
                tiempo_cafe <= tiempo_cafe_next;
                estado_leche_actual <= estado_leche_next;
                led_leche_i <= led_leche_next_i;
                leche_actual <= leche_next;
                led_leche_caliente_i <= led_leche_caliente_next_i;
                led_leche_fria_i <= led_leche_fria_next_i;
                led_azucar_i <= led_azucar_next_i;
                estado_azucar_actual <= estado_azucar_next;
                nivel_azucar <= nivel_azucar_next;
            end if;                
    end process;
 
 --estados
   -- siguiente_estado : 
    process (rst, input, estado_actual, encendido, corto, largo, leche, tiempo_cafe, tiempo_cafe_next, servido)
      begin
        estado_siguiente <= estado_actual;
        tiempo_cafe_next <= tiempo_cafe;
        
          case estado_actual is
            when inicio =>
                if (rst = '0' and encendido = '1') then
                    estado_siguiente <= reposo;
                    tiempo_cafe_next <= select_cafe;
                end if;
                
            when reposo =>
                if (input = '0') then
                    if (corto = '1' and largo = '0') then
                        tiempo_cafe_next <= cafe_corto;
                    elsif (corto = '0' and largo = '1') then
                        tiempo_cafe_next <= cafe_largo;
                    else
                        if(corto = '0' and largo = '0' and leche = '1') then
                            tiempo_cafe_next <= solo_leche;
                        else
                            tiempo_cafe_next <= select_cafe;
                        end if;
                    end if;
                 elsif (input = '1') then
                    if (tiempo_cafe_next /= select_cafe or tiempo_cafe_next = solo_leche) then
                        estado_siguiente <= dispensando;
                    end if;
                 end if;
            when dispensando =>
                if (servido = '1') then
                    tiempo_cafe_next <= select_cafe;
                  if (input = '0') then
                    estado_siguiente <= reposo;
                   end if;
                 end if;
            when others =>
                null;
         end case;
     end process;
        
--output : 
process (clk, estado_actual, corto, largo, leche, led_corto_i, led_largo_i, led_leche_i, estado_leche_actual, estado_leche_next, azucar, led_azucar_i, estado_azucar_actual, estado_azucar_next,mas_azucar, menos_azucar, nivel_azucar, nivel_azucar_next, servido)
    begin
        led_encendido_i <= '0';
        led_apagado_i <= '0';
        led_corto_i_next <= led_corto_i;
        led_largo_i_next <= led_largo_i;
        estado_leche_next <= estado_leche_actual;
        led_leche_next_i <= led_leche_i;
        led_azucar_next_i <= led_azucar_i;
        estado_azucar_next <= estado_azucar_actual;
        nivel_azucar_next <= nivel_azucar;
        
        case estado_actual is
            when inicio =>
                led_encendido_i <= '0';
                led_apagado_i <= '1';
                led_corto_i_next <= '0';
                led_largo_i_next <= '0';
                led_leche_next_i <= '0';
                led_azucar_next_i <= '0';
                binC <= "1010";
                binL <= "1010";
                binLeche <= "1010";
                binL_C_F <= "1010";
                binA <= "1010";
                binA_nivel <= "1010";
                L_6 <= "1011";
                L_7 <= "1011";
                
            when reposo =>
                led_encendido_i <= '1';
                led_apagado_i <= '0';
                L_6 <= "1011";
                L_7 <= "1011";
                
                --corto
                if (corto = '1') then
                    led_corto_i_next <= '1';
                    binC <= "1101";
                else
                    led_corto_i_next <= '0';
                    binC <= "1010";
                end if;
                
                --largo
                if (largo = '1') then
                    led_largo_i_next <= '1';
                    binL <= "1100";
                else
                    led_largo_i_next <= '0';
                    binL <= "1010";
                end if;
                
                --leche
                if (leche = '1') then
                    led_leche_next_i <= '1';
                    binLeche <= "1100";
                    if leche_caliente = '1' and leche_fria='0' then
                        led_leche_caliente <='1';
                        led_leche_fria <= '0';
                        binL_C_F <= "1101";
                    elsif leche_caliente = '0' and leche_fria='1' then
                        binL_C_F <= "1111";
                        led_leche_caliente <='0';
                        led_leche_fria <= '1';
                        
                    else
                        binL_C_F <= "1010";
                    end if;
                else
                    led_leche_next_i <= '0';
                    led_leche_caliente <='0';
                    led_leche_fria <= '0';
                    estado_leche_next <= inicio_leche;
                    binLeche <= "1010";
                    binL_C_F <= "1010";
                end if;
                
                --azucar
                if (azucar = '1') then
                    led_azucar_next_i <= '1';
                    binA <= "1001";
                    
                    case estado_azucar_actual is
                        when inicio_azucar =>
                            nivel_azucar_next <= 1;
                            estado_azucar_next <= select_azucar;
                        
                        when select_azucar =>
                            if (mas_azucar = '1' and menos_azucar = '0') then
                                if (nivel_azucar = 1) then
                                    nivel_azucar_next <= 2;
                                elsif (nivel_azucar = 2) then
                                    nivel_azucar_next <= 3;
                                end if;
                            elsif (mas_azucar = '0' and menos_azucar = '1') then
                                if (nivel_azucar = 3) then
                                    nivel_azucar_next <= 2;
                                elsif (nivel_azucar = 2) then
                                    nivel_azucar_next <= 1;
                                end if;
                            end if;                           
                       when others => NULL;
                    end case;
                    
                    case nivel_azucar is
                        when 1 =>
                            binA_nivel <= "0001";
                        when 2 =>
                            binA_nivel <= "0010";
                        when 3 =>
                            binA_nivel <= "0011";
                        when others =>
                            binA_nivel <= "1010";
                    end case;
                    
                else
                    binA<= "1010";
                    binA_nivel<="1010";
                    led_azucar_next_i <= '0';
                    nivel_azucar_next <= 0;
                    estado_azucar_next <= inicio_azucar;
                end if;
                    
            when dispensando =>
                led_encendido_i <= '1';
                binC <= "1000"; --E
                binL <= "0111"; --S
                binLeche <= "0110"; --P
                binL_C_F <= "1000"; --E
                binA <= "0101"; --r
                binA_nivel <= "1000";   --E
                L_6 <= "1011";
                L_7 <= "1011";
                
                if (servido = '1') then
                    estado_leche_next <= inicio_leche;
                    estado_azucar_next <= inicio_azucar;
                    led_apagado_i <= '0';
                    
                    
                else
                    led_apagado_i <= '1';
                    
                end if;
            when others =>
                null;
        end case;
    end process;
    
    led_encendido <= led_encendido_i;
    led_corto <= led_corto_i;
    led_largo <= led_largo_i;
    led_leche <= led_leche_i; 
    led_azucar <= led_azucar_i;   
    led_apagado <= led_apagado_i;
    
--contador : 
process(clk, estado_actual, estado_siguiente, tiempo_cafe, count, servido)  
      begin
        if (estado_actual = reposo) then
            servido <= '0';
                case tiempo_cafe is
                    when inicio_cafe => count <= 0;
                    when select_cafe => count <= 0;
--                    when cafe_corto => count <= 1000000000; --10s
                    when cafe_corto => count <= 10000; --10s
                    when cafe_largo => count <= 20000; --20s
                    when solo_leche => count <= 5000;  --5s
                    when others => null;
                end case;
           
         elsif (estado_actual = dispensando) then
            if (clk = '0' and clk'event) then
                count <= count-1;
                servido <= '0';
                if (count <= 0) then --sincronizacion de retrasos
                    servido <= '1';
                end if;
            end if;
         end if;
      end process;
      
      L_0<=binC;
      L_1<=binL;
      L_2<=binLeche;
      L_3<=binL_C_F;
      L_4<=binA;
      L_5<=binA_nivel;
      
    
              
    end Behavioral;
