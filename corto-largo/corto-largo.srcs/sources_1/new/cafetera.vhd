
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity cafetera is
    PORT ( clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           input : in STD_LOGIC; --transiciones, start
           encendido : in STD_LOGIC;
           corto : in STD_LOGIC;
           largo : in STD_LOGIC;
           leche : in STD_LOGIC;
           leche_fria: in std_logic;
           leche_caliente: in std_logic;
           azucar : in STD_LOGIC;
           mas_azucar : in STD_LOGIC;
           menos_azucar : in STD_LOGIC;
           led_corto : out STD_LOGIC;
           led_largo : out STD_LOGIC;
           led_leche : out STD_LOGIC;
           led_azucar: out std_logic;
           led_encendido : out STD_LOGIC;
           led_apagado : out STD_LOGIC;
           led_leche_caliente: out STD_LOGIC;
           led_leche_fria: out STD_LOGIC;
           D_NUMBER:   out std_logic_vector(6 downto 0);
           D_SELECT:   out std_logic_vector(7 downto 0)
          );
end cafetera;

architecture Behavioral of cafetera is
    
    component clk_divider
        generic (divisor: integer := 1000);  -- Numero por el que desea dividir los 100MHz
            Port ( 
                    INTERNAL_CLK:           in std_logic;
                    RESET:           in std_logic;
                    OUTPUT_CLK:    out std_logic
                 );
    end component;
    
    component debouncer
        PORT ( clk : in STD_LOGIC;
               boton_in : in STD_LOGIC;
               boton_out : out STD_LOGIC 
        );
    end component;
        
    component fsm
        PORT ( clk : in STD_LOGIC;
               rst : in STD_LOGIC;
               input : in STD_LOGIC; --transiciones, start
               encendido : in STD_LOGIC;
               corto : in STD_LOGIC;
               largo : in STD_LOGIC;
               leche : in STD_LOGIC;
               leche_fria : in STD_LOGIC;
               leche_caliente : in STD_LOGIC; 
               azucar : in STD_LOGIC;
               mas_azucar : in STD_LOGIC;
               menos_azucar : in STD_LOGIC;
               led_corto : out STD_LOGIC;
               led_largo : out STD_LOGIC;
               led_leche : out STD_LOGIC;
               led_leche_caliente : out STD_LOGIC;
               led_leche_fria : out STD_LOGIC;
               led_azucar : out STD_LOGIC;
               led_encendido : out STD_LOGIC;
               led_apagado : out STD_LOGIC;
               L_0 : out STD_LOGIC_VECTOR(3 downto 0); --corto
               L_1 : out STD_LOGIC_VECTOR(3 downto 0); --largo
               L_2 : out STD_LOGIC_VECTOR(3 downto 0); --leche
               L_3 : out STD_LOGIC_VECTOR(3 downto 0); --leche fria
               L_4 : out STD_LOGIC_VECTOR(3 downto 0); --leche caliente
               L_5 : out STD_LOGIC_VECTOR(3 downto 0); --azucar nivel 1
               L_6 : out STD_LOGIC_VECTOR(3 downto 0); --azucar nivel 2
               L_7 : out STD_LOGIC_VECTOR(3 downto 0)  --azucar nivel 3
        );
    end component;
    
    component display_refresh
    port (  clk:        in std_logic;
            reset:      in std_logic;
            L_0:        in std_logic_vector(6 downto 0);
            L_1:        in std_logic_vector(6 downto 0);
            L_2:        in std_logic_vector(6 downto 0);
            L_3:        in std_logic_vector(6 downto 0);
            L_4:        in std_logic_vector(6 downto 0);
            L_5:        in std_logic_vector(6 downto 0);
            L_6:        in std_logic_vector(6 downto 0);
            L_7:        in std_logic_vector(6 downto 0);
            D_NUMBER:   out std_logic_vector(6 downto 0);
            D_SELECT:   out std_logic_vector(7 downto 0)
         );
    end component;
    
    component decoder
        port ( CODE : in std_logic_vector (3 downto 0);
		       LED : out std_logic_vector (6 downto 0)
		     );
    end component;
  
 signal clk_fsm, clk_display : STD_LOGIC;
 signal milk : STD_LOGIC_VECTOR (3 downto 0);
 signal input_debounce, mas_azucar_debounce, menos_azucar_debounce : STD_LOGIC;
 signal L0, L1, L2, L3, L4, L5, L6, L7 : STD_LOGIC_VECTOR (3 downto 0);
 signal segmen_L0, segmen_L1, segmen_L2, segmen_L3, segmen_L4, segmen_L5, segmen_L6, segmen_L7 : STD_LOGIC_VECTOR (6 downto 0); 
 
 
 begin
 
    inst_clk_divider: clk_divider
        generic map (divisor => 50000) --2000 Hz a fsm
            port map (
                INTERNAL_CLK => clk,
                RESET => rst,
                OUTPUT_CLK => clk_fsm
                );        
    inst_clk_divider_display: clk_divider
        generic map ( divisor => 100000) --1000 Hz a displays
            port map (
                INTERNAL_CLK => clk,
                RESET => rst,
                OUTPUT_CLK => clk_display
                );   
    inst_debounce_input: debouncer
        port map (
            clk => clk_fsm,
            boton_in => input,
            boton_out => input_debounce
            );
            
     inst_debounce_mas_azucar: debouncer
        port map (
            clk => clk_fsm,
            boton_in => mas_azucar,
            boton_out => mas_azucar_debounce
        );
        
     inst_debounce_menos_azucar: debouncer
        port map (
            clk => clk_fsm,
            boton_in => menos_azucar,
            boton_out => menos_azucar_debounce
        );
        
            
    inst_fsm: fsm
        port map (
               clk => clk_fsm,
               rst => rst,
               input => input_debounce,
               encendido => encendido,
               corto => corto,
               largo =>largo,
               leche => leche,
               leche_fria => leche_fria,
               leche_caliente => leche_caliente,
               azucar => azucar,
               mas_azucar => mas_azucar_debounce,
               menos_azucar => menos_azucar_debounce,
               led_corto => led_corto,
               led_largo => led_largo,
               led_leche => led_leche,
               led_leche_caliente => led_leche_caliente,
               led_leche_fria => led_leche_fria,
               led_azucar => led_azucar,
               led_encendido => led_encendido,
               led_apagado => led_apagado,
               L_0 => L0,
               L_1 => L1,
               L_2 => L2,
               L_3 => L3,
               L_4 => L4,
               L_5 => L5,
               L_6 => L6,
               L_7 => L7
        );
   
   inst_display_refresh: display_refresh
        port map (
            clk => clk_display,
            reset=> rst,
            L_0 => segmen_L0,
            L_1 => segmen_L1,
            L_2 => segmen_L2,
            L_3 => segmen_L3,
            L_4 => segmen_L4,
            L_5 => segmen_L5,
            L_6 => segmen_L6,
            L_7 => segmen_L7,
            D_NUMBER => D_NUMBER,
            D_SELECT => D_SELECT
            );
  inst_decoder_corto: decoder --primer display
        port map (
                    CODE => L0,
                    LED => segmen_L0 
                  );  
  inst_decoder_largo: decoder --segundo display
        port map (
                    CODE => L1,
                    LED => segmen_L1 
                  );
  inst_decoder_leche: decoder --tercer display
        port map (
                    CODE => L2,
                    LED => segmen_L2 
                  );
                  
  inst_decoder_LC: decoder --cuarto display
        port map (
                    CODE => L3,
                    LED => segmen_L3 
                  );
  inst_decoder_LF: decoder --quinto display
        port map (
                    CODE => L4,
                    LED => segmen_L4 
                  );
                  
  inst_decoder_az1: decoder --sexto display
        port map (
                    CODE => L5,
                    LED => segmen_L5 
                  );
  inst_decoder_az2: decoder --septimo display
        port map (
                    CODE => L6,
                    LED => segmen_L6 
                  );
                  
  inst_decoder_az3: decoder
        port map (
                    CODE => L7,
                    LED => segmen_L7 
                  );                                     
end behavioral;