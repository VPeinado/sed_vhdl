----------------------------------------------------------------------------------
-- CLK DIVIDER
-- El funcionamiento consiste en un divisor que se declarar� como generico para elegir el numero por el que dividir
-- el reloj interno de la placa de 100MHz. El proceso sumar� uno al contador cada vez que se detecte un flanco positivo del
-- reloj interno, evaluando antes si el contador ha llegado al valor del divisor. Cuando el contador llegue al valor del divisor,
-- la salida se negar� para crear un flanco en el reloj de salida y el contador se resetear� a 0.
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_divider is
    generic (divisor: integer := 5);  -- Numero por el que desea dividir los 100MHz
  Port ( 
        INTERNAL_CLK:  in std_logic;
        RESET:         in std_logic;
        OUTPUT_CLK:    out std_logic
    );
end clk_divider;

architecture Behavioral of clk_divider is
    signal counter:      integer range 0 to divisor;
    signal OUTPUT_CLK_i: std_logic := '0';  --Se�al para cambiar valor de salida
begin
	freq_divider: process (INTERNAL_CLK , RESET)
	BEGIN
	   if (RESET = '1') then --RESET se�al prioritaria
		   counter <= 0;
		   OUTPUT_CLK_i <= '0';
	   elsif rising_edge (INTERNAL_CLK) then   --Flanco positivo de reloj interno
	       if (counter = divisor - 1) then
		       counter <= 0;
		       OUTPUT_CLK_i <= not(OUTPUT_CLK_i);
		   else
		       counter <= counter + 1;
		   end if;
        end if;
    end process;
	OUTPUT_CLK <= OUTPUT_CLK_i ;
end Behavioral;
