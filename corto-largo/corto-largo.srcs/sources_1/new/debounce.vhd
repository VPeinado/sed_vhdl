----------------------------------------------------------------------------------
--DEBOUNCE (ANTIRREBOTES)
--
--
--
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity debounce is
    PORT (  clk : in STD_LOGIC;
            rst : in STD_LOGIC;
            boton_in : in STD_LOGIC;
            boton_out : out STD_LOGIC
          );
end debounce;

architecture Behavioral of debounce is

    constant cuenta_max : integer := 2000000; --20 ms ser� el tiempo maximo que se puede pulsar el boton
    constant boton_activo : STD_LOGIC := '1'; --el boton se pondra a 1 si crea un flanco positivo cuando se le presiona, si no ser� 0
    signal cuenta : integer := 1;
    
    type tipo_estado is (tiempo_espera, parado);
    signal estado_debounce : tipo_estado := parado;
begin

    boton_debounce: process (clk,rst)
    begin
        if (rst = '1') then
            estado_debounce <= parado;
            boton_out <= '0';
            
        elsif (clk = '1' and clk'event) then
            case estado_debounce is
                
                when parado =>
                    if (boton_in = boton_activo) then
                        estado_debounce <= tiempo_espera;
                    else
                        estado_debounce <= parado;
                    end if;
                    boton_out <= '0';
                    
                when tiempo_espera =>
                    if (cuenta = cuenta_max) then
                        if (boton_in = boton_activo) then
                            boton_out <= '1';
                            estado_debounce <= tiempo_espera;
                        else
                            cuenta <= 1;
                            estado_debounce <= parado;
                        end if;
                    else
                        cuenta <= cuenta+1;
                    end if;
                    
                when others => NULL;
            end case;
        end if;
    end process;        
end Behavioral;
