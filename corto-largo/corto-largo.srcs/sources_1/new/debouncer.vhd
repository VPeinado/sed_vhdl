
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity debouncer is
    Port ( 
        CLK:        in std_logic;
        BOTON_IN:   in std_logic;
        BOTON_OUT:  out std_logic
    );
end debouncer;

architecture Behavioral of debouncer is
component edgedtctr is
    Port ( CLK : in STD_LOGIC;
           SYNC_IN : in STD_LOGIC;
           EDGE : out STD_LOGIC
         );
end component;
 
component synchronizer is
    Port ( CLK : in STD_LOGIC;
           ASYNC_IN : in STD_LOGIC;
           SYNC_OUT : out STD_LOGIC
         );
end component;

signal boton_sinc: std_logic;
begin
  Inst_synchronizer: synchronizer port map (
    CLK => CLK,
    ASYNC_IN => BOTON_IN ,
    SYNC_OUT => boton_sinc
    
  );
  Inst_edgedtctr: edgedtctr port map (
    CLK => CLK,
    SYNC_IN => boton_sinc ,
    EDGE => BOTON_OUT
    
  );

end Behavioral;
