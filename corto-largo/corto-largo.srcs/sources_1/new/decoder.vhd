----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity decoder is
    PORT(
        CODE : in std_logic_vector (3 downto 0);
		LED : out std_logic_vector (6 downto 0)
		);
end decoder;

architecture Behavioral of decoder is

    signal SEGMENT : std_logic_vector (6 downto 0);

BEGIN

    with CODE select
        -- GFEDCBA LED Order
        SEGMENT <=   "1000000" WHEN "0000", -- "0"
					 "1111001" WHEN "0001", -- "1"
					 "0100100" WHEN "0010", -- "2"
					 "0110000" WHEN "0011", -- "3"
					 "1001111" WHEN "0100", -- "I"
					 "0101111" WHEN "0101", -- "r"
					 "0001100" WHEN "0110", -- "P"
					 "0010010" WHEN "0111", -- "S"
					 "0000110" WHEN "1000", -- "E"
					 "0001000" WHEN "1001", -- "A"
					 "0111111" WHEN "1010", -- "-"
					 "1111111" WHEN "1011", -- " "
					 "1000111" WHEN "1100", -- "L"
                     "1000110" WHEN "1101", -- "C"
                     "0000110" WHEN "1110", -- "E"
					 "0001110" WHEN "1111", -- "F"
					 "1111111" WHEN others; -- " "
        LED <= SEGMENT;
                
end Behavioral;
