----------------------------------------------------------------------------------
-- DISPLAY_REFRESH
-- Su funcionamiento consiste en un contador que va contando desde 0 hasta 7 (porque hay 8 displays)
-- entonces cuando esta en 0 selecciona el display 0 y representa el numero representado en L_0 en el siguiente ciclo de reloj
-- suma uno al contador por tanto vale 1 y pasa a representar el n�mero L_1 en el segmento uno, y as� sucesivamente.
-- Ocurre a alta frecuencia por tanto no se percibe que se apaguen los displays. Tambi�n tiene un boton de reset que apaga 
-- todos los displays y apaga todos los segmentos.
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity display_refresh is
  Port ( 
        CLK:        in std_logic;
        RESET:      in std_logic;
        L_0:        in std_logic_vector(6 downto 0);
        L_1:        in std_logic_vector(6 downto 0);
        L_2:        in std_logic_vector(6 downto 0);
        L_3:        in std_logic_vector(6 downto 0);
        L_4:        in std_logic_vector(6 downto 0);
        L_5:        in std_logic_vector(6 downto 0);
        L_6:        in std_logic_vector(6 downto 0);
        L_7:        in std_logic_vector(6 downto 0);
        D_NUMBER:   out std_logic_vector(6 downto 0);
        D_SELECT:   out std_logic_vector(7 downto 0)
    );
end display_refresh;

architecture Behavioral of display_refresh is
    signal s_select_rst: std_logic_vector(7 downto 0) := "11111111";
    signal s_number_rst: std_logic_vector(6 downto 0) := "1111111";    
begin
    display_refresh: process(CLK,RESET,s_select_rst,s_number_rst,L_0,L_1,L_2,L_3,L_4,L_5,L_6,L_7)
        variable refresh_counter : integer range 0 to 7;
        
    begin
        D_SELECT <= "11111111";
        D_NUMBER <= "1111111";
        
        if RESET = '1' then
            D_SELECT <= s_select_rst;
            D_NUMBER <= s_number_rst;
            refresh_counter := 0;
        
        elsif CLK'event and CLK = '1' then
            if refresh_counter = 7 then
                refresh_counter := 0;
            else
                refresh_counter := refresh_counter + 1;
            end if;
            
            case refresh_counter is
                when 0 =>   D_SELECT <= "11111110";
                            D_NUMBER <= L_0;
                                                   
                when 1 =>   D_SELECT <= "11111101";
                            D_NUMBER <= L_1;
                            
                when 2 =>   D_SELECT <= "11111011";
                            D_NUMBER <= L_2;
                                                   
                when 3 =>   D_SELECT <= "11110111";
                            D_NUMBER <= L_3;
                            
                when 4 =>   D_SELECT <= "11101111";
                            D_NUMBER <= L_4;
                                                   
                when 5 =>   D_SELECT <= "11011111";
                            D_NUMBER <= L_5;

                when 6 =>   D_SELECT <= "10111111";
                            D_NUMBER <= L_6;
                                                   
                when 7 =>   D_SELECT <= "01111111";
                            D_NUMBER <= L_7;
                
                when OTHERS => NULL;
            
            end case;
        end if;
    end process;

end Behavioral;
