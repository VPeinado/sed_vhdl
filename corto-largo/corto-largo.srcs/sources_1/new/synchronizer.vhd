----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12.11.2020 22:21:24
-- Design Name: 
-- Module Name: synchronizer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity synchronizer is
    Port ( CLK : in STD_LOGIC;
           ASYNC_IN : in STD_LOGIC;
           SYNC_OUT : out STD_LOGIC
         );
end synchronizer;

architecture Behavioral of synchronizer is
  signal sreg : std_logic_vector(1 downto 0);
begin
  process(CLK)
  begin
    if rising_edge(CLK) then
      sync_out <= sreg(1);
      sreg <= sreg(0) & async_in;
    end if;
  end process;
end Behavioral;
